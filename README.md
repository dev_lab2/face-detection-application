# Face-Detection-Application

## How to create virtaul environment (linux/macos)

```sh
python -m venv venv
source ./venv/bin/activate
```

## How to create virtaul environment (windows)

```sh
python -m venv venv
source .\venv\Script\activate
```

## How to run

```sh
pip install -r requirements.txt

streamlit run test.py

streamlit run app.py

```

## How to allow chrome to run

```sh
chrome://flags/#unsafely-treat-insecure-origin-as-secure
```

## build docker image

```sh
docker build -t <image name>:<image version> .
```

## docker run

```sh
docker run --name <container name> -p xxxx:yyyy <image name>:<image version>
```