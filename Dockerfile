FROM python:3.11-slim
WORKDIR /app
COPY . .
RUN apt-get update && apt-get -y install libgl1 libglib2.0-0
RUN pip install -r requirements.txt
ENTRYPOINT ["streamlit", "run", "test.py", "--server.port=8501", "--server.address=0.0.0.0"]